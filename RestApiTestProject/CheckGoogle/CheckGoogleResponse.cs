﻿using Allure.Commons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using RestSharp;
using System.Collections.Generic;
using System.Net;

namespace RestApiTestProject.CheckGoogle
{
    public class CheckGoogleResponse
    {
        System.Net.HttpStatusCode OK = System.Net.HttpStatusCode.OK;

        [Test(Description = "CheckGoogleStatus")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CheckGoogleStatus()
        {
            Assert.AreEqual(OK, CheckStatus200("http://www.google.com/"));
        }
        private System.Net.HttpStatusCode CheckStatus200(string url)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest
                                                       .Create(url);
            webRequest.AllowAutoRedirect = false;
            HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
            return response.StatusCode;
        }
    }
}
