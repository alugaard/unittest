﻿using Allure.Commons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using RestSharp;
using System.Collections.Generic;

namespace RestApiTestProject.SkcBerentalTests
{
    [TestFixture]
    [AllureNUnit]
    public class CheckBasicApiResponse
    {
        string apiUrl;
        System.Net.HttpStatusCode OK = System.Net.HttpStatusCode.OK;
        int errorCode = 0;
        IRestResponse restResponse;

        public CheckBasicApiResponse()
        {
            apiUrl = Utils.GetApiUrlOf("ApiSkcBerental");
        }

        [Test(Description = "DatosCliente Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void DatosCliente()
        {
            restResponse = GetResponse("DatosCliente");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CentroCosto Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CentroCosto()
        {
            restResponse = GetResponse("CentroCosto");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "ModeloGenerico Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void ModeloGenerico()
        {
            restResponse = GetResponse("ModeloGenerico");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Parque Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Parque()
        {
            restResponse = GetResponse("Parque");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "TipoCotizacion Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void TipoCotizacion()
        {
            restResponse = GetResponse("TipoCotizacion");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "EstadosBerental Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void EstadosBerental()
        {
            restResponse = GetResponse("EstadosBerental");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Faenas Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Faenas()
        {
            restResponse = GetResponse("Faenas");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CotizacionesEncabezado Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CotizacionesEncabezado()
        {
            restResponse = GetResponse("CotizacionesEncabezado");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "ImplementacionAFI Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void ImplementacionAFI()
        {
            restResponse = GetResponse("ImplementacionAFI");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Sucursal Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Sucursal()
        {
            restResponse = GetResponse("Sucursal");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Modelo Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Modelo()
        {
            restResponse = GetResponse("Modelo");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Vendedor Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Vendedor()
        {
            restResponse = GetResponse("Vendedor");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CotizacionDetalle Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CotizacionDetalle()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("IdSolicitud", "354831");

            restResponse = GetResponse("CotizacionDetalle", parameters);
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Cotizacion Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Cotizacion()
        {
            restResponse = GetResponse("Cotizacion");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "MiFlotaUsados Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void MiFlotaUsados()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("Rut", "76215876");

            restResponse = GetResponse("MiFlotaUsados", parameters);
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Login Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Login()
        {
            restResponse = GetResponse("Login");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Feriados Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Feriados()
        {
            restResponse = GetResponse("Feriados");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "TiemposST Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void TiemposST()
        {
            restResponse = GetResponse("TiemposST");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "TipoImplementacion Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void TipoImplementacion()
        {
            restResponse = GetResponse("TipoImplementacion");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "EstadosContrato Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void EstadosContrato()
        {
            restResponse = GetResponse("EstadosContrato");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "DTE Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void DTE()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("Folio", "128106");
            parameters.Add("TipoDoc", "33");
            parameters.Add("sk_session", "SKCDEV");

            restResponse = GetResponse("DTE", parameters);
            Assert.AreEqual(OK, restResponse.StatusCode);
        }

        [Test(Description = "EquiposEuModelosArr Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void EquiposEuModelosArr()
        {
            restResponse = GetResponse("EquiposEuModelosArr");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Kpi Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Kpi()
        {
            restResponse = GetResponse("Kpi");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "MiFlotaArriendo Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void MiFlotaArriendo()
        {
            restResponse = GetResponse("MiFlotaArriendo");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "DetalleEstadoArrendado Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void DetalleEstadoArrendado()
        {
            restResponse = GetResponse("DetalleEstadoArrendado");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "DetalleAsistenciaVigente Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void DetalleAsistenciaVigente()
        {
            restResponse = GetResponse("DetalleAsistenciaVigente");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "HojaResumenContrato Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void HojaResumenContrato()
        {
            restResponse = GetResponse("HojaResumenContrato");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "Pagos Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void Pagos()
        {
            restResponse = GetResponse("Pagos");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "DetallePendienteEmisionST Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void DetallePendienteEmisionST()
        {
            restResponse = GetResponse("DetallePendienteEmisionST");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "DetallePendienteEmisionARR Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void DetallePendienteEmisionARR()
        {
            restResponse = GetResponse("DetallePendienteEmisionARR");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CabeceraPendienteEmision Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CabeceraPendienteEmision()
        {
            restResponse = GetResponse("CabeceraPendienteEmision");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "DetalleDocPorCobrar Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void DetalleDocPorCobrar()
        {
            restResponse = GetResponse("DetalleDocPorCobrar");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "EstadoCuenta Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void EstadoCuenta()
        {
            restResponse = GetResponse("EstadoCuenta");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CabeceraCtaCte Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CabeceraCtaCte()
        {
            restResponse = GetResponse("CabeceraCtaCte");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "TipoDocumento Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void TipoDocumento()
        {
            restResponse = GetResponse("TipoDocumento");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "DescargablesSharePoint Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void DescargablesSharePoint()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("AFI", "2112364");
            parameters.Add("Contrato", "77714");

            restResponse = GetResponse("DescargablesSharePoint", parameters);
            Assert.AreEqual(OK, restResponse.StatusCode);
        }

        [Test(Description = "CorreosAPC Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CorreosAPC()
        {
            restResponse = GetResponse("CorreosAPC");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "FechaCompromiso Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void FechaCompromiso()
        {
            restResponse = GetResponse("FechaCompromiso");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CCAFICliente Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CCAFICliente()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("Rut", "92434000");

            restResponse = GetResponse("CCAFICliente", parameters);
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CCCargo Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CCCargo()
        {
            restResponse = GetResponse("CCCargo");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CCContacto Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CCContacto()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("Rut", "92434000");

            restResponse = GetResponse("CCContacto", parameters);
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CCAsunto Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CCAsunto()
        {
            restResponse = GetResponse("CCAsunto");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CCAreaAtencion Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CCAreaAtencion()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("BR_idArea", null);
            parameters.Add("BR_idAsunto", "1");

            restResponse = GetResponse("CCAreaAtencion", parameters);
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "CCAreaSugerencia Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void CCAreaSugerencia()
        {
            restResponse = GetResponse("CCAreaSugerencia");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "EstadoReclamo Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void EstadoReclamo()
        {
            restResponse = GetResponse("EstadoReclamo");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "EncabezadoReclamos Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
        public void EncabezadoReclamos()
        {
            restResponse = GetResponse("EncabezadoReclamos");
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "DetalleReclamos Basic Get")]
        [AllureSeverity(SeverityLevel.critical)]
            public void DetalleReclamos()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("nReclamo", "124649");

            restResponse = GetResponse("DetalleReclamos", parameters);
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        [Test(Description = "IngresaReclamo Basic Post")]
        [AllureSeverity(SeverityLevel.critical)]
        public void IngresaReclamo()
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            parameters.Add("Sucursal", "1");
            parameters.Add("Problema", "problema");
            parameters.Add("Contacto", "");
            parameters.Add("NvoContacto", "13615");
            parameters.Add("CargoContacto", "");
            parameters.Add("Telefono", "123123123");
            parameters.Add("AFI", "3003030");
            parameters.Add("FechaCreacion", "2019-07-15");
            parameters.Add("Usuario", "asalazarc");
            parameters.Add("Rut", "78721730");
            parameters.Add("Asunto", "5");
            parameters.Add("Area", "3");
            parameters.Add("EsCliente", "si");
            parameters.Add("NumDocTributario", "12345");
            parameters.Add("Sociedad", "");
            parameters.Add("AreaSugerencia", "2");
            parameters.Add("Descripcion", "");
            parameters.Add("Revision", "");
            parameters.Add("Tipo", "");

            restResponse = GetResponse("IngresaReclamo", parameters, Method.POST);
            Assert.AreEqual(OK, restResponse.StatusCode);
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        private IRestResponse GetResponse(string apiName)
        {
            return GetResponse(apiName, null);
        }

        private int GetErrorCodeFromResponse(IRestResponse restResponse)
        {
            string response = restResponse.Content;
            IDictionary<string, JToken> jsonResponse = (IDictionary<string, JToken>)JsonConvert.DeserializeObject(response);
            int errorValue = jsonResponse["error"].Value<int>();

            return errorValue;
        }

        private IRestResponse GetResponse(string apiName, Dictionary<string, string> parameters)
        {
            return GetResponse(apiName, parameters, Method.GET);
        }

        private IRestResponse GetResponse(string apiName, Dictionary<string, string> parameters, RestSharp.Method httpMethod)
        {
            RestClient restClient = new RestClient(apiUrl);
            RestRequest restRequest = new RestRequest("/api/" + apiName, httpMethod);
            restRequest.AddHeader("sk_session", "SKCDEV");
            restRequest = AsignarParametros(parameters, restRequest);

            restResponse = restClient.Execute(restRequest);
            return restResponse;
        }

        private RestRequest AsignarParametros(Dictionary<string, string> parameters, RestRequest request)
        {
            if (parameters == null)
                return request;

            foreach (KeyValuePair<string, string> p in parameters)
            {
                request.AddParameter(p.Key, p.Value);
            }
            return request;
        }
    }
}
