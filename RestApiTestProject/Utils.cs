﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace RestApiTestProject
{
    public class Utils
    {
        public static string GetApiUrlOf(string apiName)
        {
            try
            {
                using (StreamReader r = new StreamReader("Config/Config.json"))
                {
                    string json = r.ReadToEnd();
                    var items = (JObject)JsonConvert.DeserializeObject(json);
                    return items[apiName].Value<string>();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
