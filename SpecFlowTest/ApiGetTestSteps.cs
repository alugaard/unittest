﻿using Example;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace SpecFlowTest
{
    [Binding]
    public class ApiGetTestSteps
    {
        private string urlGlobal;
        IRestResponse restResponse;
        System.Net.HttpStatusCode OK = System.Net.HttpStatusCode.OK;

        [Given(@"the url (.*)")]
        public void GivenTheUrl(string url)
        {
            urlGlobal = url;
        }

        [When(@"I call the api (.*)")]
        public void WhenICallTheApi(string apiName)
        {
            restResponse = GetResponse(apiName);
        }

        [Then(@"the status response should be OK")]
        public void ThenTheResponseStatusCodeShouldBeOK()
        {
            Assert.AreEqual(OK, restResponse.StatusCode);
        }

        [Then(@"the error code should be (.*)")]
        public void AndTheErrorCodeShouldBe(int errorCode)
        {
            Assert.AreEqual(errorCode, GetErrorCodeFromResponse(restResponse));
        }

        private IRestResponse GetResponse(string apiName)
        {
            return GetResponse(apiName, null);
        }

        private int GetErrorCodeFromResponse(IRestResponse restResponse)
        {
            string response = restResponse.Content;
            IDictionary<string, JToken> jsonResponse = (IDictionary<string, JToken>)JsonConvert.DeserializeObject(response);
            int errorValue = jsonResponse["error"].Value<int>();

            return errorValue;
        }

        private IRestResponse GetResponse(string apiName, Dictionary<string, string> parameters)
        {
            RestClient restClient = new RestClient(urlGlobal);
            RestRequest restRequest = new RestRequest("/api/" + apiName, Method.GET);
            restRequest.AddHeader("sk_session", "SKCDEV");
            restRequest = AsignarParametros(parameters, restRequest);

            restResponse = restClient.Execute(restRequest);
            return restResponse;
        }

        private RestRequest AsignarParametros(Dictionary<string, string> parameters, RestRequest request)
        {
            if (parameters == null)
                return request;

            foreach (KeyValuePair<string, string> p in parameters)
            {
                request.AddParameter(p.Key, p.Value);
            }
            return request;
        }
    }
}
