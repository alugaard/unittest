﻿Feature: ApiGetTest
	Verify if apis are working

@BasicTest
Scenario Outline: Check status OK and error equal to 0
	Given the url http://localhost:26101
	When I call the api <apiName>
	Then the status response should be OK
	And  the error code should be 0

Scenarios: 
		| apiName                    |
		| CabeceraCtaCte             |
		| CabeceraPendienteEmision   |
		| CentroCosto                |
		| CorreosAPC                 |
		| Cotizacion                 |
		| CotizacionesEncabezado     |
		| DatosCliente               |
		| DetalleAsistenciaVigente   |
		| DetalleDocPorCobrar        |
		| DetalleEstadoArrendado     |
		| DetallePendienteEmisionARR |
		| DetallePendienteEmisionST  |
		| EquiposEuModelosArr        |
		| EstadoCuenta               |
		| EstadosBerental            |
		| EstadosContrato            |
		| Faenas                     |
		| FechaCompromiso            |
		| Feriados                   |
		| HojaResumenContrato        |
		| ImplementacionAFI          |
		| Kpi                        |
		| Login                      |
		| MiFlotaArriendo            |
		| MiFlotaUsados              |
		| Modelo                     |
		| ModeloGenerico             |
		| Pagos                      |
		| Parque                     |
		| Sucursal                   |
		| TiemposST                  |
		| TipoCotizacion             |
		| TipoDocumento              |
		| TipoImplementacion         |
		| Vendedor                   |



@JustStatusOKTest
Scenario Outline: Check status OK
	Given the url http://localhost:26101
	When I call the api <apiName>
	Then the status response should be OK

Scenarios: 
		| apiName                    |
		| DescargablesSharePoint     |
		| DTE					     |
		| Reportes                   |
